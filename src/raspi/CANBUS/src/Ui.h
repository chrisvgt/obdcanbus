#ifndef UI_H
#define UI_H

#include <iostream>
#include <sstream>

#include "Utils.h"
#include "Frame.h"

class Ui
{
private:
    std::stringstream ss;

public:
    Ui();
    ~Ui();
    /**
     * @brief add welcome heading
     * @note add to stringtream ss
     * 
     */
    void welcome();

    /**
     * @brief add try connection
     * @note add to stringtream ss
     * 
     * @param str 
     */
    void tryConnection(std::string str);

    /**
     * @brief add connection successfull
     * @note add to stringtream ss
     * 
     * @param str 
     */
    void connected(std::string str);

    /**
     * @brief add dashboard heading
     * @note add to stringtream ss
     * 
     */
    void dashboard();

    /**
     * @brief add frame data
     * @note add to stringtream ss
     * 
     * @param Frame 
     */
    void frame(Frame &Frame);

    /**
     * @brief add decoded data
     * @note add to stringtream ss
     * 
     * @param str 
     * @param val 
     */
    void decodedData(std::string str, int val);

    /**
     * @brief clear stringstream ss
     * 
     */
    void resetSS();

    /**
     * @brief print stringstream ss and clear console
     * 
     */
    void clearPrint();

    /**
     * @brief print stringstream ss
     * 
     */
    void print();
};

#endif /* UI_H */
