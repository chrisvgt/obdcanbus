#ifndef DECODING_H
#define DECODING_H

#include <map>
#include <string>
#include <stdint.h>

#include "Frame.h"

class Decoding
{
private:
    std::map<std::string, int> decodedData;

public:
    Decoding();
    ~Decoding();

    /**
     * @brief decode received can data with obd2pids
     * @link https://en.wikipedia.org/wiki/OBD-II_PIDs
     * @param Frame 
     */
    void decode(Frame &Frame);

    /**
     * @brief get the decoded data object
     * 
     * @return std::map<std::string, int> decoded data
     */
    std::map<std::string, int> getDecodedData();

    /**
     * @brief delete key entry from decodedData
     * @note  entry deletion after successful transfer to db
     * @param key unique key
     */
    void eraseEntry(std::string key);
};

#endif /* DECODING_H */