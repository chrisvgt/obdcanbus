#ifndef UTILS_H
#define UTILS_H

#include <iostream>
#include <cstring>
#include <string>
#include <sstream>
#include <vector>

#if defined _WIN32
    #include <conio.h>
#endif

/**
 * @brief convert buffer to string
 * 
 * @note buffer needs to be cleaned, to avoid data[0] = 0
 * @note str: '\000' <repeats 15 times>, "\061\062,100,200,44,144" 
 * @note removes '\n' from string 
 * 
 * @param buf
 * @param len length of the buffer
 * @return std::string 
 */
std::string bufferToString(char *buf, int len);

/**
 * @brief convert string to string vector
 * 
 * @param str string
 * @param delimiter delimiter char e.g. ','
 * @return std::vector<std::string> 
 */
std::vector<std::string> stringToVector(std::string str, char delimiter);

/**
 * @brief convert string vector to int vector
 * 
 * @param numbers string vector with numbers
 * @return std::vector<int> 
 */
std::vector<int> stringToInt(std::vector<std::string> numbers);

/**
 * @brief clears terminal
 * 
 */
void clear();
#endif