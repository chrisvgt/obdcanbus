#ifndef MAIN_H
#define MAIN_H

#include <iostream>

/* 
    InfluxDB Client API
    https://github.com/offa/influxdb-cxx 
*/
#include "../lib/influxdb-cxx-0.6.5/include/Point.h"
#include "../lib/influxdb-cxx-0.6.5/include/InfluxDB.h"
#include "../lib/influxdb-cxx-0.6.5/include/Transport.h"
#include "../lib/influxdb-cxx-0.6.5/include/InfluxDBFactory.h"
#include "../lib/influxdb-cxx-0.6.5/include/InfluxDBException.h"

#include "Ui.h"
#include "Utils.h"
#include "UartConnection.h"
#include "Decoding.h"

void canbus(Ui &Ui, Frame &Frame, Decoding &Decoding, UartConnection &UartConnection);

#endif /* MAIN_H */