#include "Decoding.h"

Decoding::Decoding()
{
}

Decoding::~Decoding()
{
}

void Decoding::decode(Frame &Frame)
{
    const uint8_t &ID = Frame.getID();
    const uint8_t &A = Frame.getA();
    const uint8_t &B = Frame.getB();
    const uint8_t &C = Frame.getC();
    const uint8_t &D = Frame.getD();

    switch (ID)
    {
    case 0x0C: /* engine speed */
        decodedData["EngSpeed"] = (((256 * A) + B) / 4);
        break;

    case 0x0D: /* vehicle speed */
        decodedData["VehSpeed"] = A;
        break;

    case 0x0F: /* intake air temperature */
        decodedData["InAirTemp"] = A - 40;
        break;

    case 0x10: /* MAF */
        decodedData["MAF"] = ((256 * A) + B) / 100;
        break;

    case 0x5C: /* engine oil temperature  */
        decodedData["EngOilTemp"] = A - 40;
        break;

    case 0x33: /* absolute barometric pressure */
        decodedData["AbsBarPress"] = (((256 * A) + B) / 100);
        break;

    case 0x5E: /* engine fuel rate */
        decodedData["EngFuelRate"] = (((256 * A) + B) / 20);
        break;

    default:
        break;
    }
}

std::map<std::string, int> Decoding::getDecodedData()
{
    return this->decodedData;
}

void Decoding::eraseEntry(std::string key)
{
    this->decodedData.erase(key);
}