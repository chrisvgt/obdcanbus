#ifndef FRAME_H
#define FRAME_H

#include <iostream>
#include <vector>
#include <stdint.h>

class Frame
{
private:
    uint8_t ID; /* DATA2 */
    uint8_t A;  /* DATA3 */
    uint8_t B;  /* DATA4 */
    uint8_t C;  /* DATA5 */
    uint8_t D;  /* DATA6 */

public:
    Frame();
    ~Frame();

    /**
     * @brief set the Frame object
     * 
     * @param data vector with data about frame
     * @note e.g. valid EngSpeed frame
     * @note DATA2,DATA3,DATA4,DATA5,DATA6,DATA7
     * @note 0x0C ,0x0E ,0xB5 ,0x00 ,0x00 ,FILL
     */
    void setFrame(std::vector<int> data);

    /* setter */

    void setID(uint8_t id);
    void setA(uint8_t a);
    void setB(uint8_t b);
    void setC(uint8_t c);
    void setD(uint8_t d);

    /* getter */

    uint8_t getID();
    uint8_t getA();
    uint8_t getB();
    uint8_t getC();
    uint8_t getD();
};

#endif /* FRAME_H */