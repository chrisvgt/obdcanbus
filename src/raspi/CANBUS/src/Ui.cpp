#include "Ui.h"

Ui::Ui()
{
}

Ui::~Ui()
{
}

void Ui::welcome()
{
    ss << "########################\r\n"
       << "######## CANBUS ########\r\n"
       << "########################\r\n"
       << "-------------------------------------------------------\r\n"
       << "CANBUS is about to start ... \r\n";
}

void Ui::tryConnection(std::string str)
{
    ss << "Try to connect to " << str << std::endl;
}

void Ui::connected(std::string str)
{
    ss << "Successfully connected to " << str << std::endl;
}

void Ui::dashboard()
{
    ss << "########################\r\n"
       << "######## CANBUS ########\r\n"
       << "########################\r\n"
       << "-------------------------------------------------------\r\n";
}

void Ui::frame(Frame &Frame)
{
    ss << "\t\tID\tA\tB\tC\tD\r\n"
       << "Current Frame: "
       << "\t" << int(Frame.getID())
       << "\t" << int(Frame.getA())
       << "\t" << int(Frame.getB())
       << "\t" << int(Frame.getC())
       << "\t" << int(Frame.getD())
       << std::endl
       << "-------------------------------------------------------\r\n"
       << std::endl;
}

void Ui::decodedData(std::string str, int val)
{
    ss << str << "\t" << val << std::endl;
}

void Ui::resetSS()
{
    ss.str("");
}

void Ui::clearPrint()
{
    clear();
    std::cout << ss.str() << std::endl;
    resetSS();
}

void Ui::print()
{
    std::cout << ss.str() << std::endl;
    resetSS();
}
