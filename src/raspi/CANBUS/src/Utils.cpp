#include "Utils.h"

std::string bufferToString(char *buf, int len)
{
    std::string str,tmp;
    for (int i = 0; i < len; i++)
    {
        /* dont add zeros */
        if (buf[i] <= 0) 
        {
            continue;
        }
        else
        {
            tmp = buf[i];

            /* erase \n from string */
            if (!tmp.empty() && tmp[tmp.length() - 1] == '\n')
            {
                tmp.erase(tmp.length() - 1);
            }
            str += tmp;
        }
    }
    return str;
}

std::vector<std::string> stringToVector(std::string str, char delimiter)
{
    std::stringstream ss;
    ss << str;

    std::vector<std::string> result;
    while (ss.good())
    {
        std::string substr;
        getline(ss, substr, delimiter);
        result.push_back(substr);
    }
    return result;
}

std::vector<int> stringToInt(std::vector<std::string> numbers)
{
    std::vector<int> result;
    for (int i = 0; i < numbers.size(); i++)
    {
        int num = atoi(numbers.at(i).c_str());
        result.push_back(num);
    }
    return result;
}

void clear()
{
#if defined _WIN32
    clrscr();
#elif defined (__LINUX__) || defined(__gnu_linux__) || defined(__linux__)
    system("clear");
#elif defined (__APPLE__)
    system("clear");
#endif
}

