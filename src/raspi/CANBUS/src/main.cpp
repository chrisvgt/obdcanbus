#include "main.h"

int main(int argc, char *argv[])
{
    Ui Ui;
    Frame Frame;
    Decoding Decoding;
    UartConnection UartConnection;

    clear();
    Ui.welcome();
    Ui.print();
    sleep(2);

    int count = 0;
    int maxTries = 3;
    UartConnection.initUart();
    try
    {
        canbus(Ui, Frame, Decoding, UartConnection);
    }
    catch (const char *&e)
    {
        if (++count == maxTries)
        {
            std::cerr << e << std::endl;
            exit(2);
        }
        UartConnection.initUart();
        canbus(Ui, Frame, Decoding, UartConnection);
    }
    return 0;
}

void canbus(Ui &Ui, Frame &Frame, Decoding &Decoding, UartConnection &UartConnection)
{
    while (UartConnection.isConnected())
    {
        UartConnection.readUart();
        if (UartConnection.getStr().length() >= 9)
        {
            Frame.setFrame(stringToInt(stringToVector(UartConnection.getStr(), ',')));
            Decoding.decode(Frame);

            Ui.dashboard();
            Ui.frame(Frame);

            for (auto elem : Decoding.getDecodedData())
            {
                try
                {
                    //[protocol]://[username:password@]host:port[?db=database]
                    auto influxdb = influxdb::InfluxDBFactory::Get("http://canbus:htwberlin@localhost:8086?db=canbus");
                    influxdb->write(influxdb::Point{"canbus"}
                                        .addField(elem.first, elem.second)
                                        .addTag("host", "localhost"));
                    Ui.decodedData(elem.first, elem.second);
                    //Decoding.eraseEntry(elem.first);
                }
                catch (const std::exception &e)
                {
                    std::cerr << e.what() << '\n';
                    sleep(2);
                }
            }
            Ui.clearPrint();
        }
    }
    throw "Disconnected ...";
};