#ifndef UARTCONNECTION_H
#define UARTCONNECTION_H

#include <stdio.h>
#include <sys/types.h>
#include <termios.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

#include <iostream>
#include <string>
#include <sstream>

#include "Utils.h"
#include "Frame.h"

class UartConnection
{
private:
    int fd;
    char buf[32];
    std::string str;

    bool status = false;
    struct termios serial_settings;
    std::string SERIAL_DEV = "/dev/ttyAMA0";

public:
    
    /**
     * @brief construct a new uartconnection object
     * 
     */
    UartConnection();

    /**
     * @brief destroy the uartconnection object
     * 
     */
    ~UartConnection();

    /**
     * @brief initialize uart communciation
     * 
     * @return int 
     */
    int initUart();

    /**
     * @brief reads data from uart
     *
     */
    void readUart();

    /**
     * @brief closes the uart connection
     * 
     * @return int 
     */
    int closeUart();

    /**
     * @brief check if serial connection is established
     * 
     * @return true 
     * @return false 
     */
    bool isConnected();

    /**
     * @brief get the SERIAL_DEV
     * 
     * @return std::string 
     */
    std::string getSerialDev();

    /**
     * @brief set the SERIAL_DEV 
     * 
     * @param str 
     */
    void setSerialDev(std::string str);

    /**
     * @brief get the str
     * 
     * @return std::string 
     */
    std::string getStr();
};

#endif /* UARTCONNECTION_H */
