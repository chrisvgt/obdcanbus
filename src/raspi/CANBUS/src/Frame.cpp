#include "Frame.h"

Frame::Frame()
{
}

Frame::~Frame()
{
}

void Frame::setFrame(std::vector<int> data)
{
    this->ID = data[0];
    this->A = data[1];
    this->B = data[2];
    this->C = data[3];
    this->D = data[4];
}

void Frame::setID(uint8_t id)
{
    this->ID = id;
}

void Frame::setA(uint8_t a)
{
    this->A = a;
}

void Frame::setB(uint8_t b)
{
    this->B = b;
}

void Frame::setC(uint8_t c)
{
    this->C = c;
}

void Frame::setD(uint8_t d)
{
    this->D = d;
}

/* getter */

uint8_t Frame::getID()
{
    return this->ID;
}

uint8_t Frame::getA()
{
    return this->A;
}

uint8_t Frame::getB()
{
    return this->B;
}

uint8_t Frame::getC()
{
    return this->C;
}

uint8_t Frame::getD()
{
    return this->D;
}