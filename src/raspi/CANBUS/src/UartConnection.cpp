#include "UartConnection.h"
#include "Ui.h"

UartConnection::UartConnection()
{
}

UartConnection::~UartConnection()
{
}

int UartConnection::initUart()
{
    Ui Ui;
    int count = 0;
    int maxTries = 5;
    while (true)
    {
        try
        {
            Ui.tryConnection(SERIAL_DEV);
            Ui.print();
            fd = open(getSerialDev().c_str(), O_RDWR | O_NOCTTY);
            if (fd < 0)
            {
                throw "failed to open SERIAL_DEV";
            }

            if (tcgetattr(fd, &serial_settings) < 0)
            {
                throw "termios structure failed";
            }

            /* set baud rate to B115200 */
            if (cfsetospeed(&serial_settings, B115200) < 0)
            {
                throw "failed to set 115200 output baud rate";
            }
            if (cfsetispeed(&serial_settings, B115200) < 0)
            {
                throw "failed to set 115200 input baud rate";
            }

            serial_settings.c_lflag |= ICANON; /* use canonical input */

            /* disabling ECHO */
            serial_settings.c_lflag &= ~(ECHO | ECHOE);
            if (tcsetattr(fd, TCSANOW, &serial_settings) < 0)
            {
                throw "serial attributes failed";
            }

            status = true;
            Ui.connected(SERIAL_DEV);
            Ui.print();
            return 0;
        }
        catch (const char *&e)
        {
            std::cerr << e << std::endl;
            sleep(5);
            clear();
            Ui.dashboard();
            if (++count == maxTries)
            {
                std::cerr << e << std::endl;
                exit(1);
            }
        }
    }
}

void UartConnection::readUart()
{
    bzero(buf,32);
    str = "";

    int len = read(fd, buf, sizeof(buf));
    if (len < 0)
    {
        str = "";
        status = false;
    }
    else if (len > 1) /* minimum length for data */
    {
        status = true;
        str = bufferToString(buf, len);
    }
    status = true;
}

int UartConnection::closeUart()
{
    return close(fd);
}

bool UartConnection::isConnected()
{
    return status;
}

std::string UartConnection::getSerialDev()
{
    return SERIAL_DEV;
}

void UartConnection::setSerialDev(std::string str)
{
    SERIAL_DEV = str;
}

std::string UartConnection::getStr()
{
    return str;
}