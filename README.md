The CANBUS project is intended to **establish communication with CAN-capable devices** or vehicles (e.g.: cars) and to **read data from them**, as well as process them and make them available to the user.

For this purpose, the printed circuit board created in the summer semester 2020 will be used in this project, and the appropriate components for this purpose. More about this topic can be found in the Wiki.

circuit board  
<img src="https://gitlab.com/chrisvgt/obdcanbus/-/raw/master/doc/img/hardware.png" width="50%">

From a hardware point of view, the printed circuit board must be equipped with individual parts and the communication between this and the simulation boards must be established via physical connections. 
the simulation boards via physical interconnections. In addition it requires for the communication still at
- Software that has to be written and implemented,
- protocols for the STM and Raspi,
- application software to store and display the data.

Besides the requirements and goals of the self-selected project, there are also the goals
of the module itself as a project. 

In the foreground here is the approach to software and hardware projects, in particular the *Project management*.
and the development of the basics of these, should represent a basic competence at the end of the semester.

test setup with example data  
<img src="https://gitlab.com/chrisvgt/obdcanbus/-/raw/master/doc/img/example.gif" width="80%">

